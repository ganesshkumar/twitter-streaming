import threading 

from Queue import Queue
from TwitterListener import TwitterListener

count = 0
threadLock = threading.Lock()
tweets_queue = Queue()

def read():
    global count
    while True:
        threadLock.acquire()
        item = tweets_queue.get()
        threadLock.release()
        count += 1
        print count
        print item


if __name__ == '__main__':
    print('ready')
    listener = TwitterListener(tweets_queue)
    listener.listen(['manutd'])
    print('listener initialized')

    thread = threading.Thread(target=read)
    thread.daemon = True
    thread.start()
    thread.join()
